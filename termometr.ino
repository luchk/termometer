#include <OneWire.h>
#include <DallasTemperature.h>
#include <LiquidCrystal.h>
#define  ONE_WIRE_BUS 2


LiquidCrystal lcd(8, 9, 7, 6, 5, 4);
OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);

void setup(void)
{
  lcd.begin(16, 2);
  // start serial port
  Serial.begin(9600);
  sensors.begin();
}


void loop(void)
{
  sensors.requestTemperatures();
  Serial.print("Temperature for Device is: ");
  Serial.print(sensors.getTempCByIndex(0));
  float b = sensors.getTempCByIndex(0);
  lcd.setCursor(0,0);
  lcd.print("Temperature:");
  lcd.setCursor(0,1);
  lcd.print(b);
}
